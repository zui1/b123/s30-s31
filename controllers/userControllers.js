const User = require('../models/User');

module.exports.createUserController = (req, res) => {

    console.log(req.body);

    User.findOne({ username: req.body.username })
        .then(result => {

            //console.log(result);

            if (result !== null && result.username === req.body.username) {

                return res.send("Duplicate username found!")

            } else {

                let newUser = new User({

                    username: req.body.username,
                    password: req.body.password

                })

                newUser.save()
                    .then(result => res.send("Succesful Registration!"))
                    .catch(err => res.send(err))

            }

        })
        .catch(err => res.send(err))
}

module.exports.getAllUsersController = (req, res) => {

    User.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.getSingleUserController = (req, res) => {

    //mongoose has a query called findById() which works like find({_id: "id"})
    //console.log(req.params.id)//the id passed from your url
    User.findById(req.params.id)
    .then(result => res.send(result))
    .catch(err => res.send(err))

}

module.exports.updateSingleUserController = (req,res) => {

    console.log(req.body);
    console.log(req.params.id);
    /*
        Model.findByIdAndUpdate will do 2 things, first, look for the single item by its id, then add the update.

        Model.findByIdAndUpdate(id,{updates},{new:true})
        
        By default, without the third argument, findByIdAndUpdate returns the document before the updates were added.
        {new:true} - allows to return the updated document.
    */
    //updates object will contain the field and the value we want to update
    let updates = {

        username: req.body.username

    }
    User.findByIdAndUpdate(req.params.id,updates,{new:true})
    .then(updatedUser => res.send(updatedUser))
    .catch(err => res.send(err))

}