//import Task model
const Task = require('../models/Task');

module.exports.createTaskController = (req, res) => {

    console.log(req.body);
    //Create a new Task document.
    //Check if there is a document with duplicate name field.
    //Model.findOne() is a Mongoose method similar to findOne in MongoDB.
    //However, with Model.findOne(), we can process the result via our API.
    Task.findOne({ name: req.body.name })
        .then(result => {
            //.then() is able to capture the result of our query.
            //is able to process the result and when that result is returned, we can actually add another then() to process the next result.


            //console.log(result); //returns null if no documents were found with the name given in the criteria.
            //returns undefined if there is an error.

            //This will allow to send a message to the client a duplicate document was found/
            if (result !== null && result.name === req.body.name) {

                return res.send("Duplicate Task Found");

            } else {

                //Created a new task object out of our Task model.
                //newTask has added methods for use in our application.
                let newTask = new Task({

                    name: req.body.name

                })

                //.save() is a method from an object created by a model.
                //This will then allow to save our document into our collection.
                //.save() can have an anonymous function and this can take 2 parameters.
                //The first item: saveErr receives an error object if there was an error creating our document.
                //The second item is our saved document.
                newTask.save()
                    .then(result => res.send("Item Added")).catch(err => res.send(err))

            }

        })
        //.catch() is able to capture the error of our query.
        .catch(err => res.send(err))

}

module.exports.getAllTasksController = (req, res) => {

    //Model.find() is a Mongoose moethod similar to MongoDB's find(). It is able to retrieve all documents that will match the criteria. 
    Task.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.getSingleTaskController = (req, res) => {

    //console.log(req.params.id)
    //mongoose queries such as Model.find(), Model.findOne(), Model.findById() has a second argument for projection. And, how it works is the same as in mongoDB 
    Task.findById(req.params.id)
    .then(result => res.send(result))
    .catch(err => res.send(err))

}

module.exports.updateSingleTaskToCompleteController = (req,res) => {

    console.log(req.params.id);
   
    let updates = {

        status: "complete"

    }
    Task.findByIdAndUpdate(req.params.id,updates,{new:true})
    .then(updatedUser => res.send(updatedUser))
    .catch(err => res.send(err))

}

module.exports.updateSingleTaskToCancelController = (req,res) => {

    console.log(req.params.id);
   
    let updates = {

        status: "cancel"

    }
    Task.findByIdAndUpdate(req.params.id,updates,{new:true})
    .then(updatedUser => res.send(updatedUser))
    .catch(err => res.send(err))

}