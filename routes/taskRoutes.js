const express = require("express");
//Router() from express, allows to access to HTTP method routes.
//Router() will action as a middleware and our routing system.
const router = express.Router();
//Routes should only be concerned with our endpoints and our methods.
//The action to be done once a route is accessed should be in separate file, it should be in our controllers.
const taskControllers = require('../controllers/taskControllers');
const {createTaskController,getAllTasksController, getSingleTaskController, updateSingleTaskToCompleteController, updateSingleTaskToCancelController} = taskControllers

router.post('/', createTaskController);

router.get('/', getAllTasksController);

router.get('/:id', getSingleTaskController)

router.put('/complete/:id', updateSingleTaskToCompleteController);

router.put('/cancel/:id', updateSingleTaskToCancelController);

//router holds all of our routes and can be exported and imported into another file.
module.exports = router;