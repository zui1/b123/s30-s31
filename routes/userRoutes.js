const express = require("express");

const router = express.Router();

const userControllers = require('../controllers/userControllers');

const { createUserController, 
        getAllUsersController, 
        getSingleUserController, 
        updateSingleUserController 
    } = userControllers

router.post('/', createUserController);

router.get('/', getAllUsersController);

//get single user route
router.get('/:id', getSingleUserController);

//update single user
router.put('/:id', updateSingleUserController);

module.exports = router;